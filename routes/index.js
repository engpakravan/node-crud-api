const express = require('express');
const router = express.Router();

const privateAPI = require(`routes/${process.env.API_VERSION}/private`)
const publicAPI = require(`routes/${process.env.API_VERSION}/public`)

router.use(`/api/${process.env.API_VERSION}`,
  //TODO Add Private Middleware For HERE
  privateAPI)

router.use(`/api/${process.env.API_VERSION}`,publicAPI)

 module.exports = router;